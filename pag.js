var database = [{"id":1,"first_name":"Charlton","last_name":"Crickett","email":"ccrickett0@skype.com","gender":"Male","avatar":"https://robohash.org/dolordebitistempore.jpg?size=150x150&set=set1","friends":[349,823,435,67,638,20]},
{"id":2,"first_name":"Dodie","last_name":"Goodanew","email":"dgoodanew1@aol.com","gender":"Female","avatar":"https://robohash.org/quiasintet.bmp?size=150x150&set=set1","friends":[974,563,365,989,988,943,910]},
{"id":3,"first_name":"Merrili","last_name":"Searchwell","email":"msearchwell2@nyu.edu","gender":"Female","avatar":"https://robohash.org/repellatquidemaccusantium.png?size=150x150&set=set1","friends":[864,887,527,894]},
{"id":4,"first_name":"Bette","last_name":"Ody","email":"body3@slate.com","gender":"Female","avatar":"https://robohash.org/autnonaut.jpg?size=150x150&set=set1","friends":[501,235,177]},
{"id":5,"first_name":"Ginni","last_name":"Pietruschka","email":"gpietruschka4@wisc.edu","gender":"Female","avatar":"https://robohash.org/etrecusandaeodio.jpg?size=150x150&set=set1","friends":[888,783,927,942,63,552]},
{"id":6,"first_name":"Zahara","last_name":"Sandison","email":"zsandison5@tripadvisor.com","gender":"Female","avatar":"https://robohash.org/cupiditateaccusantiumeligendi.jpg?size=150x150&set=set1","friends":[413,968,114,424,626,522]},
{"id":7,"first_name":"Gav","last_name":"Hosier","email":"ghosier6@studiopress.com","gender":"Male","avatar":"https://robohash.org/maioresitaqueea.bmp?size=150x150&set=set1","friends":[10,913,414,835,81]},
{"id":8,"first_name":"Aaren","last_name":"Clifford","email":"aclifford7@ihg.com","gender":"Female","avatar":"https://robohash.org/voluptatesomnisvitae.jpg?size=150x150&set=set1","friends":[258,629]},
{"id":9,"first_name":"Nadine","last_name":"Adamik","email":"nadamik8@eventbrite.com","gender":"Female","avatar":"https://robohash.org/quidemvoluptatemducimus.bmp?size=150x150&set=set1","friends":[277,194,347,760,290,898]},
{"id":10,"first_name":"Ansley","last_name":"Malimoe","email":"amalimoe9@icq.com","gender":"Female","avatar":"https://robohash.org/veniamdistinctiosed.bmp?size=150x150&set=set1","friends":[538,379,519,216,64]}];

var pagination = new Pagination(database, 3);
pagination.init();

$('.btn').click(function(){
  var pageNumber = $(this).text();
  $('.btn').removeClass('active');
  $(this).addClass('active');
  pagination.renderRobot(pageNumber);
});

function Pagination(database,itemsPerPage){
    this.db = database;
    this.itemsPerPage = itemsPerPage;
    this.init = function(){
    this.renderRobot(1);
    this.renderPagination();
  },
  this.renderRobot = function(page){
    
    var endIndex = this.itemsPerPage * page;
    var currentIndex = endIndex - this.itemsPerPage;
    
    $("#robots").empty();
    
    var database = this.db;
    
    for(var i = currentIndex; i < endIndex; i++){
      var robot = database[i];
      var robotId = "robotId" + robot.id;
      var robotDiv = '<div class="robot card" id="' + robotId + '"> </div>';
      $('#robots').append(robotDiv);

      var avatar = '<img  src="'+ robot.avatar +'" />';
      $("#" + robotId).append(avatar);

      var username = '<h4>' + robot.first_name + " " + robot.last_name + "</h4>";
      $("#" + robotId).append(username);

    }
  },
  this.renderPagination = function(){
    
    var amountOfPages = Math.ceil(this.db.length/this.itemsPerPage);
    
    $("#pagination").append('<ul class="pagination"></ul>');

    for(var i = 0; i < amountOfPages; i++){
      var pageButton = '<button type="button" class="btn btn-link">'+ (i+1) +'</button>';
      $('.pagination').append('<li>' + pageButton + '</li>');
    }
  }
} 
