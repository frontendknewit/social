var count = 1000;
var cnt = 12;
var cnt_page = Math.ceil(count / cnt);

var paginator = document.querySelector(".paginator");
var page = "";
for (var i = 0; i < cnt_page; i++) {
	page += "<a data-page=" + i * cnt + " id=\"page" + (i+1) + "\">" + (i+1) + "</a>";
}
paginator.innerHTML = page;

var div_item = document.getElementsByClassName("item");
for(var i = 0; i < div_item.length; i++) {
	if(i < cnt) {
		div_item[i].style.display = "inline-block";
	}
}

var main_page = document.getElementById('page1');
main_page.classList.add("paginator_active");

function pagination(event) {
	var e = event || window.event;
	var target = e.target;
	var id = target.id;

	if (target.tagName.toLowerCase() != "a") return;

	var num_ = id.substr(4);
	var data_page = +target.dataset.page;
	main_page.classList.remove("paginator_active");
	main_page = document.getElementById(id);
	main_page.classList.add("paginator_active");

	var j = 0;
	for (var i = 0; i < div_item.length; i++) {
		var data_item = div_item[i].dataset.item;
		if(data_item <= data_page || data_item >= data_page) 
			div_item[i].style.display = "none";
	}

	for (var i = data_page; i < div_item.length; i++) {
		if (j >= cnt) break;
		div_item[i].style.display = "inline-block";
		j++;
	}
}