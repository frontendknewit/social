$(document).ready(function() {

    //initialize swiper when document ready  
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        autoplay: 2000,
        // Navigation arrows
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev'
    });

    var database;



    $.getJSON("db/database.json", function(data) {
        database = data;
        $("#counterContainer").text(database.length);
        $("#loader").hide();
        renderRobots(database);
    });






    //поиск пользователя
    $("#name").keyup(function() {

        var search = $(this).val();

        var found = false;

        for (var i = 0; i < database.length; i++) {
            if (search == database[i].first_name || (database[i].first_name.indexOf(search) != -1) && search != "") {
                found = true;
            } else {
                $("#robotId" + database[i].id).css('display', 'none'); //остальные убирает
            }
        }

        if (!found) {
            $("div[id^='robotId']").css('display', 'inline-block'); //найденные
        }
    });




    var inputValue = document.getElementById('name').value;

    document.getElementById('deleteById').onclick = function() {
        var value = document.getElementById('deleteId').value;

        if (!deleteFromDB(database, value)) {
            alert('Не нашел!!');
        }
    }

    //удаление по значку
    $(".delete").click(function() {
        var user_id = $(this).attr('user_id');

        if (!deleteFromDB(database, user_id)) {
            alert('Not found');
        }
    });

    //добавление
    document.getElementById('add_user').onclick = function() {

        $(".add").submit(function(event) {
            event.preventDefault();
        });

        var name = document.getElementById('add_name').value;
        var email = document.getElementById('add_email').value;
        var gender = document.getElementById('add_gender').value;
        var photo = document.getElementById('photo_link').value;
        var fr_links = document.getElementById('friends_link').value;

        for (var i = 0; i < database.length; i++) {
            if (name == database[i].first_name) {
                alert('Пользователь с таким именем уже существует!');
            }
            if (email == database[i].email) {
                alert('Этот эмейл уже занят!');
            }
        };

        database.push({ id: database.length + 1, first_name: name, email: email, gender: gender, avatar: photo, friends: fr_links });

        var container = $('.usersContainer').prepend("<div class='item' id='item" + database[database.length - 1].id + "'></div>");

        $("#item" + database[database.length - 1].id).append("<button class='delete' user_id='" + database[database.length - 1].id + "'>" + 'x' + "</button>");
        $("#item" + database[database.length - 1].id).append("<h3>" + database[database.length - 1].first_name + "</h3>");
        $("#item" + database[database.length - 1].id).append("<h3> ID - " + database[database.length - 1].id + "</h3>");
        $("#item" + database[database.length - 1].id).append("<img src='" + database[database.length - 1].avatar + "'/>");
        $("#item" + database[database.length - 1].id).append("<a id='way' href='mailto:" + database[database.length - 1].email + "'>" + database[database.length - 1].email + "</a>");
        $("#item" + database[database.length - 1].id).append("<p>" + database[database.length - 1].gender + "</p>");

        var friendsName = [];

        for (var j = 0; j < database.length; j++) {
            if (database[database.length - 1].id != database[j].id && database[database.length - 1].friends.indexOf(database[j].id) != -1) {
                friendsName.push({ name: database[j].first_name, id: database[j].id });
            }
        };

        for (var j = 0; j < friendsName.length; j++) {
            $("#item" + database[database.length - 1].id).append("<a href='#item" + friendsName[j].id + "'>" + friendsName[j].name + "</a>");
        }

        $("#counterContainer").text(database.length);
    }

    //удаление
    function deleteFromDB(db, id) {

        var found = false;

        for (var i = 0; i < database.length; i++) {
            if (id == database[i].id) {
                $("#robotId" + database[i].id).hide('slow');
                database.splice(i, 1);
                $("#counterContainer").text(db.length);
                found = true;
                break;
            }
        };
        return found;
    }

    // function renderRobots(database){

    // 	var container = $('#counterContainer').append("<h1 id='counter'> " + database.length + " </h1>");

    // 	for(var i = 0; i < database.length; i++){

    // 		var container = $('.usersContainer').append("<div class='item' id='item" + database[i].id + "'></div>");

    // 		$("#item" + database[i].id).append("<button class='delete' user_id='" + database[i].id +  "'>" + 'x' + "</button>");
    // 		$("#item" + database[i].id).append("<h3>" + database[i].first_name +  " " + database[i].last_name +   "</h3>");
    // 		$("#item" + database[i].id).append("<h3> ID - " + database[i].id + "</h3>");
    // 		$("#item" + database[i].id).append("<img src='"+ database[i].avatar +"'/>");
    // 		$("#item" + database[i].id).append("<a id='way' href='mailto:"+ database[i].email +"'>" + database[i].email +   "</a>");
    // 		$("#item" + database[i].id).append("<p>" + database[i].gender +  "</p>");

    // var friendsName = [];

    // for (var j = 0; j < database.length; j++) {
    // 	if(database[i].id != database[j].id && database[i].friends.indexOf(database[j].id) != -1){
    // 		friendsName.push({name: database[j].first_name,id: database[j].id});
    // 	}
    // };

    // for(var j = 0; j < friendsName.length; j++){
    // 	$("#item" + database[i].id).append("<a href='#item" + friendsName[j].id +"'>" + friendsName[j].name + "</a>");	
    // }
    // 	}
    // }

    //PAGINATION
    function renderRobots(database) {
        var pagination = new Pagination(database, 12);
        pagination.init();


        $('.btn-link').click(function() {
            var pageNumber = $(this).text();
            $('.btn-link').removeClass('active');
            $(this).addClass('active');
            pagination.renderRobot(pageNumber);
        });

        function Pagination(database, itemsPerPage) {
            this.db = database;
            this.itemsPerPage = itemsPerPage;
            this.init = function() {
                    this.renderRobot(1);
                    this.renderPagination();
                },
                this.renderRobot = function(page) {
                    var endIndex = this.itemsPerPage * page;
                    var currentIndex = endIndex - this.itemsPerPage;

                    $('#robots').empty();

                    var database = this.db;

                    for (var i = currentIndex; i < endIndex; i++) {
                        var robot = database[i];
                        var robotId = 'robotId' + robot.id;
                        var robotDiv = '<div class="item" id="' + robotId + '"></div>';
                        $('#robots').append(robotDiv);

                        var delBtn = '<button class="delete" userId="' + database[i].id + '">' + "x" + '</button>';
                        $("#" + robotId).append(delBtn);

                        var avatar = '<img src="' + robot.avatar + '" />';
                        $("#" + robotId).append(avatar);

                        var username = '<h4>' + robot.first_name + " " + robot.last_name + '</h4>';
                        $("#" + robotId).append(username);

                        var id = '<h4>' + database[i].id + '</h4>';
                        $("#" + robotId).append(id);

                        var male = '<p>' + database[i].gender + '</p>';
                        $("#" + robotId).append(male);

                        var mail = '<a id="way" href="mailto:' + database[i].email + '">' + database[i].email + '</a>';
                        $("#" + robotId).append(mail);
                    }
                },

                this.renderPagination = function() {
                    var amountOfPages = Math.ceil(this.db.length / this.itemsPerPage);
                    $('#page').append('<div id="pagination"></div>');
                    $("#pagination").append('<ul class="pagination"></ul>');
                    $('.pagination').append('<button type="button" class="btn-left">' + '<<' + '</button>');
                    $('.pagination').append('<button type="button" class="btn-right"> ' + '>>' + '</button>');

                    for (var i = 0; i < amountOfPages; i++) {
                        var pageButton = '<button type="button" class="btn btn-link">' + (i + 1) + '</button>';
                        $('.pagination').append('<li>' + pageButton + '</li>');
                    }


                }
        }



    }





});