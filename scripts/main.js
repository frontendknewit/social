console.log('second');
function sortByHeight(a) {

	// [-1, 150, 190, 170, -1, -1, 160, 180]
	// [-1, 150, 170, 160, -1, -1, 180, 190]
    
	for(var i = 0; i < a.length; i++){
	    for(var i = 0; i < a.length; i++){

	        if(a[i] == -1){ continue;}
	        
	        var nextObject = i+1;



	        // if next item is not the last
	        if(nextObject != a.length){

	        	//if current man's height is bigger than next man's
				if(a[i] > a[i+1] && a[nextObject] != -1){
				            
		            var swapValue = a[i];
		            a[i] = a[nextObject];
		            a[nextObject] = swapValue;

		        } else if(a[nextObject] == -1){
		            
		       
		            while(a[nextObject] == -1){
		                if(nextObject + 1 != a.length){
		                	nextObject++;
		                } else {
		                	break;
		                }
		            }

		            //if current man's height is bigger than next man's
					if(a[i] > a[nextObject] && a[nextObject] != -1){
					            
			            var swapValue = a[i];
			            a[i] = a[nextObject];
			            a[nextObject] = swapValue;
			            i = nextObject - 1;
			        } 
		            
		        }

	        } 
	       
	    }
	  }	
    return a;
}